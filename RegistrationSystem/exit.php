<?php 
	session_start();
	require_once('functions.php');
	$functions = new Functions;
	if ( isset($_SESSION['user']) ){
		session_destroy();
		$functions->{'AlertAndRedirect'}('Logout efetuado com sucesso!','index.php');
	}else{
		$functions->{'AlertAndRedirect'}('Você não está logado!','index.php');
	}
?>