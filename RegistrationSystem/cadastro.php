<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Cadastro</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <!-- <a class="navbar-brand" href="#">Registration system</a> -->
      <span class="navbar-brand glyphicon glyphicon-floppy-saved"></span>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
         <li class="active"><a href="cadastro.php">Cadastro</a></li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
    
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php if ( isset($_SESSION['user']) ) { echo $_SESSION['user']; }else{ echo "<span class='glyphicon glyphicon-menu-hamburger'></span>"; } ?><span class="caret"></span></a>
          <ul class="dropdown-menu">
            <?php if ( isset($_SESSION['user']) ) { ?>
              <li><a href="exit.php">Sair</a></li>
            <?php }else{ ?>
              <li><a href="login.php">Entrar</a></li>
              <li><a href="cadastro.php">Cadastrar</a></li> 
            <?php
                  }
            ?>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



<div class="col-md-3">
</div>

<div class="col-md-6">
<h1>Cadastrar-se</h1>
<form action="cadastro_function.php" method="post" accept-charset="utf-8">

        <div class="input-group input-group-lg">
          <span class="input-group-addon" id="sizing-addon1">Nome:   </span>
          <input type="text" name="nome" class="form-control" placeholder="Nome Completo" aria-describedby="sizing-addon1" required> 
        </div>
        <br />
        <div class="input-group input-group-lg">
          <span class="input-group-addon" id="sizing-addon1">Cidade:  </span>
          <input type="text" name="cidade" class="form-control" placeholder="Cidade" aria-describedby="sizing-addon1" required>
          <span class="input-group-addon" id="sizing-addon1">UF:  </span>
          <input type="text" name="estado" class="form-control" placeholder="Estado" aria-describedby="sizing-addon1" required>
        </div>
        <br />
        <div class="input-group input-group-lg">
          <span class="input-group-addon" id="sizing-addon1">CEP:     </span>
          <input type="text" pattern="^[0-9]{5}-?[0-9]{3}$" oninvalid="setCustomValidity('Seu CEP deve conter os seguintes formatos: 00000-000 ou 00000000')" Onkeyup="try{setCustomValidity('')}catch(e){}" OnKeyPress="formatar('#####-###', this)" maxlength="9" name="cep" class="form-control" placeholder="00000-000" aria-describedby="sizing-addon1" required>
        </div>
        <br />
        <div class="input-group input-group-lg">
          <span class="input-group-addon" id="sizing-addon1">E-mail:  </span>
          <input type="email" pattern="^[A-Za-z0-9._]{1,15}@[A-Za-z]{1,15}\.[A-Za-z0-9]{1,4}\.?[a-z]{1,4}?$" oninvalid="setCustomValidity('Seu E-Mail deve conter os seguintes formatos: email@domain.com ou email@domain.com.br')" Onkeyup="try{setCustomValidity('')}catch(e){}" name="email" class="form-control" placeholder="E-mail" aria-describedby="sizing-addon1" required>
        </div>
        <br />
        <div class="input-group input-group-lg">
          <span class="input-group-addon" id="sizing-addon1">Usuario:</span>
          <input type="text" name="user" class="form-control" placeholder="Usuario" aria-describedby="sizing-addon1" required>
        </div>
        <br />
        <div class="input-group input-group-lg">
          <span class="input-group-addon" id="sizing-addon1">Senha:  </span>
          <input type="password" name="password" class="form-control" placeholder="Senha" aria-describedby="sizing-addon1" required>
        </div>
        <br />
        <button type="submit" class="btn btn-default">Cadastrar</button>

      </form>
</div>

<div class="col-md-3">
</div>



<nav class="navbar navbar-default navbar-fixed-bottom">
  <div class="container">
   Developer: <a href="https://www.facebook.com/profile.php?id=100014272535689" target="_blank">Jadiael</a><br />
   &copy; Copyright 2017 - <?php echo date("Y"); ?>, Example Corporation - Todos direitos reservados
   <div class="nav navbar-nav navbar-right">
		Project Registration System: <a href="https://gitlab.com/DerexScript/WebProjects/tree/master/RegistrationSystem" target="_blank" class="fa fa-gitlab" aria-hidden="true"> GitLab</a>
    <a href="https://github.com/DerexScript/RegistrationSystem" target="_blank" class="fa fa-github" aria-hidden="true"> GitHub</a>
   </div>
  </div>
</nav>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/functions.js"></script>
  </body>
</html>