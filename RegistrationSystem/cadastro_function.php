<?php
	require_once('connection.php');
	require_once('functions.php');
	$functions = new Functions;
	//verifica se houve post de todos campos do formulario!
	if ( isset($_POST['nome']) && isset($_POST['cidade']) && isset($_POST['estado']) && isset($_POST['cep']) && isset($_POST['email']) && isset($_POST['user']) && isset($_POST['password']) ){
		// Insere os dados do registro no banco de dados!
		try {
			$InsertPDO = "INSERT INTO cadastro (nome,cidade,estado,cep,email,user,password) VALUES (:nome, :cidade, :estado, :cep, :email, :user, :password)";
			$Result = $ConnPDO->prepare($InsertPDO);
			$Result->bindParam(':nome', $_POST['nome'], PDO::PARAM_STR);
			$Result->bindParam(':cidade', $_POST['cidade'], PDO::PARAM_STR);
			$Result->bindParam(':estado', $_POST['estado'], PDO::PARAM_STR);
			$cp = str_replace('-', '', $_POST['cep']);
			$Result->bindParam(':cep', $cp, PDO::PARAM_INT);
			$Result->bindParam(':email', $_POST['email'], PDO::PARAM_STR);
			$Result->bindParam(':user', $_POST['user'], PDO::PARAM_STR);
			$pw = md5($_POST['password']);
			$Result->bindParam(':password', $pw, PDO::PARAM_STR);
			$Result->execute();
		} catch(PDOException $e) {
			 echo $e->getCode().$e->getMessage();	
		}
		$functions->{'AlertAndRedirect'}('Cadastro Realizado Com Sucesso!','index.php'); 
	}else{
		$functions->{'AlertAndRedirect'}('Você não tem permissões para isso!','index.php');
	}

?>